/*
 * mm_alloc.c
 *
 * Stub implementations of the mm_* routines.
 */

#include "mm_alloc.h"
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>


#define PAGESIZE  sysconf(_SC_PAGESIZE);

/*
 * The header type will contain metadata for the space allocation.
 * The memory requested via mm_malloc will be rounded up to number of header-sized units.
 */

typedef struct header {
    size_t size;
    bool free;
    struct header *next;
    struct header *prev;
    char ptr[0];
} Header;

/*  Empty list to get started */
static Header base;    

/* Start of the free list */
static Header *freep = NULL;


Header *malloc_init() {
     base.next = freep  = &base;
     base.prev = &base;
     base.size = 0;
     return &base;
}




/*
 * moremem(size_t nunits) 
 * Param nunits: Number of struct header sized memory units to request from OS.
 * Uses 'sbrk' sys call as internal implementation. Moves the position of break in PAGESIZE 
 * increments to avoid "No Man's Land" issue related bugs.
 */
static Header *moremem(size_t nunits) {
    char *cp;
    Header *up;
    size_t memsize;
    size_t pgsize = PAGESIZE;

    if (nunits*sizeof(Header) < pgsize ) {
        memsize = pgsize;
    } else {
        memsize = ((nunits * sizeof(Header) - 1)/pgsize + 1)*pgsize;
    } 

    cp = sbrk(memsize);
    /* Could move the break, no more memory */
    if (cp == (char *) -1)
        return NULL;

    up = (Header *)cp;
    /* sizeof(Header) is 32 bytes, memsize is multiple of PAGESIZE which is multiple of 32. 
     * Therefore we can set the length of newly allocated memory in units of sizeof(Header)
     */
    up->size = memsize/sizeof(Header);
    mm_free((void *) up->ptr);
    return freep;
}




void *mm_malloc(size_t size) {
    if (size == 0) {
        return NULL;
    }

    Header *p, *prevp;
    size_t nunits;

    nunits = (size + sizeof(Header) - 1)/sizeof(Header) + 1;
    prevp = freep;
    if (prevp == NULL) { 
        /* Malloc is called for the first time. No free list yet */
        prevp = malloc_init();
    }   

    for (p = prevp->next; ; prevp = p, p = p->next) {
        if (p->size >= nunits) {         // Big enough memory chunk is found
            if (p->size == nunits) { // Size matches exactly
                prevp->next = p->next; 
                p->next->prev = prevp;
            } else {      // Memory chink is too large, allocate block from tail end
                p->size -= nunits;
                p += p->size;
                p->size = nunits;
            }
            freep = prevp;
            p->free = false;
            memset(p->ptr, 0, (p->size - 1)*sizeof(Header));
            return (void *) (p->ptr);
        }

        /* Wrapped around freelist and did not find memory chunk of suitable size. */
        if (p == freep)
            if ((p = moremem(nunits)) == NULL)
                return NULL;    // Can not allocate anymore heap memory 
    }
}

void *mm_realloc(void *ptr, size_t size) {
    if (ptr == NULL) {
        return mm_malloc(size);
    }

    if (size == 0) {
        mm_free(ptr);
        return NULL;
    }

    size_t nunits;

    nunits = (size + sizeof(Header) - 1)/sizeof(Header) + 1;
    Header *header_pointer;
    header_pointer = (Header *)ptr - 1;
    if (header_pointer->size >= nunits) {
        return ptr;
    }

    void *new_mem = (void *) mm_malloc(size) ;
    if (new_mem == NULL)
        return NULL;

    size_t num_bytes = (header_pointer->size - 1)*sizeof(Header);
    mm_free(ptr);
    memcpy(new_mem, header_pointer->ptr, num_bytes);
    memset((char *)new_mem + num_bytes, 0, size - num_bytes);

    return new_mem;
}

void mm_free(void *ptr) {
    if (ptr == NULL) 
        return;

    Header *header_pointer, *p;
    /* Point to header of argument memory pointer */
    header_pointer = (Header *)ptr - 1; 
    header_pointer->free = true;

    for (p = freep; !(header_pointer > p && header_pointer < p->next); p = p->next) {
        if (p >= p->next && (header_pointer > p || header_pointer < p->next)) {
            // Freed block is at start or the end of usable heap
            break;
        }
    }
    /* The loop above stops when we find out that the block being freed is either sandwiched 
     * somewhere between two free blocks or is at the start or the end of the usable heap 
     */

    /* Coaslesce with next neighbouring free  block if there is one */
    if (header_pointer + header_pointer->size == p->next) {
        header_pointer->size += p->next->size;
        header_pointer->next = p->next->next;
        header_pointer->next->prev = header_pointer;
    } else {
        header_pointer->next = p->next;
        header_pointer->next->prev = header_pointer;
    }

    /* Coaslesce with prev neighbouring free block if there is one */
    if (p + p->size == header_pointer) {
        p->size += header_pointer->size;
        p->next = header_pointer->next;
        header_pointer->prev = p;
    } else {
        p->next = header_pointer;
        header_pointer->prev = p;
    }

    freep = p;
}
